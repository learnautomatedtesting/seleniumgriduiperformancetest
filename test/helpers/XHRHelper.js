import { browser } from '@wdio/globals';

class XHRhelper {
    constructor() {
        this.xhrData = [];
        this.pendingRequests = {};
        this.xhrCount = 0;
        this.lastXHRInitiationTime = 0;
        this.isXHRMonitoring = true;
        this.filterPattern = null; // No filter by default
        this.unfinishedXHRs = [];
    }

    enable() {
        browser.on('Network.requestWillBeSent', (params) => {
            if (params.type === 'XHR' && this.isXHRMonitoring && this.matchesFilter(params.request.url)) {
               // console.log('Network.requestWillBeSent detected!', params);
                
                this.xhrCount++;
                this.lastXHRInitiationTime = new Date().getTime();
                this.pendingRequests[params.requestId] = {
                    timestamp: params.timestamp,
                    headers: params.request.headers
                };

                // Track the initiation of an XHR request
                this.unfinishedXHRs.push({
                    requestId: params.requestId,
                    url: params.request.url,
                    finished: false,
                    headers: params.request.headers
                });
            }
        });

        browser.on('Network.responseReceived', (params) => {
            if (params.type === 'XHR' && this.isXHRMonitoring && this.pendingRequests[params.requestId]) {
                //console.log('Network.responseReceived detected!', params);
                this.xhrCount--;
                const requestInfo = this.pendingRequests[params.requestId];
                const duration = params.timestamp - requestInfo.timestamp;
                this.xhrData.push({
                    url: params.response.url,
                    statusCode: params.response.status,
                    duration: duration * 1000,
                    requestHeaders: requestInfo.headers
                });

                // Mark XHR as finished
                const xhrIndex = this.unfinishedXHRs.findIndex(x => x.requestId === params.requestId);
                if (xhrIndex !== -1) {
                    this.unfinishedXHRs[xhrIndex].finished = true;
                }

                delete this.pendingRequests[params.requestId];
            }
        });
    }

    disableMonitoring() {
        console.log('Disabling XHR monitoring...');
        this.isXHRMonitoring = false;
    }

    enableMonitoring() {
        console.log('Enabling XHR monitoring...');
        this.isXHRMonitoring = true;
    }

    setFilterPattern(pattern) {
        console.log('Setting filter pattern...');
        this.filterPattern = pattern;
    }

    matchesFilter(url) {
        return !this.filterPattern || url.includes(this.filterPattern);
    }

    async waitForXHRs(gracePeriod = 3000) {

    try {
        await browser.waitUntil(() => {
            const now = new Date().getTime();
            const isWaitingOver = this.xhrCount === 0 && (now - this.lastXHRInitiationTime > gracePeriod);
            return isWaitingOver;
        }, {
            timeout: 30000,
            timeoutMsg: 'XHRs did not finish within the expected time.'
        });
    }
    catch (error) {
    console.log('Continuing despite the error:', error.message);
    }

    this.unfinishedXHRs.forEach(xhr => {
        console.log({
            url: xhr.url,
            statusCode: xhr.finished ? 'Finished' : 'Not Finished',
            requestHeaders: xhr.headers
        });
    });
    
        console.log('Continuing with the script. All XHRs, finished or not, are logged.');
    }

    getXHRErrors() {
        // Implement this method based on your requirements
        // For example, filter xhrData for entries with non-200 status codes
    }

   async resetXHRData() {
        this.xhrData = [];
        this.unfinishedXHRs = [];
    }

    async waitForAllXHRsToComplete(gracePeriod) {
        const timeout = 30000; // maximum time to wait for XHRs to complete
        try {
            await browser.waitUntil(() => {
                const now = new Date().getTime();
                const isWaitingOver = this.xhrCount === 0 || (now - this.lastXHRInitiationTime > gracePeriod);
                return isWaitingOver;
            }, {
                timeout: timeout,
                timeoutMsg: 'XHRs did not finish within the expected time.'
            });
        } catch (error) {
            throw new Error('Timeout waiting for XHRs to complete');
        }
    }

    getXHRData() {
        return this.xhrData;
    }
}

export default new XHRhelper();
